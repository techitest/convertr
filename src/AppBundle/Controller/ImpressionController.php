<?php

namespace AppBundle\Controller;

use Service\LinkTracker;
use AppBundle\Entity\Impression;
use AppBundle\Entity\Link;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class ImpressionController extends Controller
{
    /**
     * @Route("/impressions", name="impressions")
     */
    public function indexAction()
    {
        $impressions = $this->getDoctrine()
            ->getRepository(Impression::class)
            ->findAll();

        return $this->render('impressions/index.html.twig', [
            'impressions' => $impressions
        ]);
    }

    /**
     * @Route("/impression", name="impression_simulate")
     */
    public function trackImpressionAction(Request $request)
    {
        $linkId = $request->query->get('link');
        $picsum = "https://picsum.photos/id/$linkId/200/300";

        $link = $this->getDoctrine()
            ->getRepository(Link::class)
            ->find($linkId);

        $doctrine = $this->getDoctrine();
        $linkTracker = new LinkTracker($link, $request, $doctrine);
        $linkTracker->trackImpression();

        return new Response(
            "<img src='{$picsum}' >"
        );
    }
}


