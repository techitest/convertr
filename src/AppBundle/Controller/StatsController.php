<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Impression;
use AppBundle\Entity\Stats;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class StatsController extends Controller
{
    /**
     * @Route("/stats", name="stats")
     */
    public function indexAction()
    {
        $stats = $this->getDoctrine()
            ->getRepository(Stats::class)
            ->findAll();

        return $this->render('stats/index.html.twig', [
            'stats' => $stats
        ]);
    }
}
