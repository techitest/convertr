<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Link;
use Service\LinkTracker;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Click;
use Symfony\Component\HttpFoundation\Response;


class ClickController extends Controller
{
    /**
     * Lists all Click entities.
     *
     * @Route("/clicks", name="clicks_index")
     *
     */
    public function indexAction()
    {
        $clicks = $this->getDoctrine()
            ->getRepository(Click::class)
            ->findAll();

        return $this->render('click/index.html.twig', [
            'clicks' => $clicks
        ]);
    }

    /**
     * @Route("/click", name="click_simulate")
     */
    public function trackClickAction(Request $request)
    {
        $linkId = $request->query->get('link');
        if (!$linkId) throw $this->createNotFoundException('Invalid link');

        $link = $this->getDoctrine()
            ->getRepository(Link::class)
            ->find($linkId);

        $em = $this->getDoctrine();
        $linkTracker = new LinkTracker($link, $request, $em);
        $linkTracker->trackClick();

        return new Response(
            "<html>
<body><h1>Thank you for visiting</h1></body></html>
"
        );
    }

    /**
     * Creates a new Click entity.
     *
     * @Route("/new", name="clicks_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $click = new Click();
        $form = $this->createForm('AppBundle\Form\ClickType', $click);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($click);
            $em->flush();

            return $this->redirectToRoute('clicks_show', array('id' => $click->getId()));
        }

        return $this->render('click/new.html.twig', array(
            'click' => $click,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Click entity.
     *
     * @Route("/clicks/{id}", name="clicks_show")
     * @Method("GET")
     */
    public function showAction(Click $click)
    {
        $deleteForm = $this->createDeleteForm($click);

        return $this->render('click/show.html.twig', array(
            'click' => $click,
            'delete_form' => $deleteForm->createView(),
        ));
    }
}
