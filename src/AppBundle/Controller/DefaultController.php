<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Link;
use AppBundle\Form\LinkType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $links = $this->getDoctrine()
            ->getRepository(Link::class)
            ->findAllWithStats();

        return $this->render('links/index.html.twig', [
            'links' => $links
        ]);
    }

    /**
     * @Route("/link/add", name="link.add")
     */
    public function linkAddAction(Request $request)
    {
        $link = new Link();
        $form = $this->createFormBuilder($link)
            ->add('name', TextType::class, ['attr' => [
                'class' => 'form-control',
                'style' => 'margin-bottom:15px'
            ]])
            ->add('destination', TextType::class, ['attr' => [
                'class' => 'form-control',
                'style' => 'margin-bottom:15px'
            ]])
            ->add('save', SubmitType::class, ['label'=>'Create Link', 'attr' => [
                'class' => 'btn btn-primary',
                'style' => 'margin-bottom:15px'
            ]])->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $name = $form['name']->getData();
            $destination = $form['destination']->getData();
            $now = new \DateTime('now');

            $link->setName($name)
                ->setDestination($destination)
                ->setCreateTs($now);

            $em = $this->getDoctrine()->getManager();
            $em->persist($link);
            $em->flush();

            $this->addFlash('success', 'Link added successfully');
            return $this->redirectToRoute('homepage');
        }

        return $this->render('links/add.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/link/edit/{id}", name="link.edit")
     */
    public function linkEditAction($id, Request $request)
    {
        $link = $this->getDoctrine()
            ->getRepository('AppBundle:Link')
            ->find($id);

        if (!$link) throw $this->createNotFoundException('This link does not exist');

        $link->setName($link->getName())
            ->setDestination($link->getDestination())
            ->setCreateTs($link->getCreateTs());

        $form = $this->createFormBuilder($link)
            ->add('name', TextType::class, ['attr' => [
                'class' => 'form-control',
                'style' => 'margin-bottom:15px'
            ]])
            ->add('destination', TextType::class, ['attr' => [
                'class' => 'form-control',
                'style' => 'margin-bottom:15px'
            ]])
            ->add('save', SubmitType::class, ['label'=>'Save Link', 'attr' => [
                'class' => 'btn btn-primary',
                'style' => 'margin-bottom:15px'
            ]])->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $name = $form['name']->getData();
            $destination = $form['destination']->getData();

            $link->setName($name)
                ->setDestination($destination);

            $em = $this->getDoctrine()->getManager();
            $em->persist($link);
            $em->flush();

            $this->addFlash('success', 'Link updated successfully');
            return $this->redirectToRoute('link.edit', [
                'id' => $id,
                'form' => $form->createView()
            ]);
        }

        // replace this example code with whatever you need
        return $this->render('links/edit.html.twig', [
            'link' => $link,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/link/delete/{id}", name="link.delete")
     */
    public function linkDeleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $link = $em->getRepository(Link::class)->find($id);

        if (!$link) return $this->redirectToRoute('homepage');

        $em->remove($link);
        $em->flush();

        $this->addFlash('success', 'Link has been deleted');
        return $this->redirectToRoute('homepage');
    }
}
