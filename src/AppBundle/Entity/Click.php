<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Click
 *
 * @ORM\Table(name="click")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ClickRepository")
 */
class Click
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createTs", type="datetime")
     */
    private $createTs;

    /**
     * @var array
     *
     * @ORM\Column(name="referringUrl", type="string", length=255)
     */
    private $referringUrl;

    /**
     * @var array
     *
     * @ORM\Column(name="ip", type="string", length=255)
     */
    private $ip;

    /**
     * @var
     *
     * @ORM\ManyToOne(targetEntity="Link", inversedBy="clicks")
     * @ORM\JoinColumn(name="linkId", referencedColumnName="id")
     *
     */
    private $link;

    public static function add(Link $link, $ip, $referringUrl, $doctrine)
    {
        $_this = (new self())
            ->setLink($link)
            ->setIp($ip)
            ->setReferringUrl($referringUrl)
            ->setCreateTs(new \Datetime);

        $em = $doctrine->getManager();
        $em->persist($_this);
        $em->flush();

        return $_this;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createTs
     *
     * @param \DateTime $createsTs
     *
     * @return Click
     */
    public function setCreateTs($createTs)
    {
        $this->createTs = $createTs;

        return $this;
    }

    /**
     * Get createTs
     *
     * @return \DateTime
     */
    public function getCreateTs()
    {
        return $this->createTs;
    }

    /**
     * Set link
     *
     * @param \AppBundle\Entity\Link $link
     *
     * @return Click
     */
    public function setLink(Link $link = null)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return \AppBundle\Entity\Link
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set ip
     *
     * @param string $ip
     *
     * @return Click
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }


    /**
     * Add click
     *
     * @param \AppBundle\Entity\Click $click
     *
     * @return Link
     */
    public function addClick(\AppBundle\Entity\Click $click)
    {
        $this->clicks[] = $click;

        return $this;
    }

    /**
     * Remove click
     *
     * @param \AppBundle\Entity\Click $click
     */
    public function removeClick(\AppBundle\Entity\Click $click)
    {
        $this->clicks->removeElement($click);
    }


    /**
     * Set referringUrl
     *
     * @param string $referringUrl
     *
     * @return Click
     */
    public function setReferringUrl($referringUrl)
    {
        $this->referringUrl = $referringUrl;

        return $this;
    }

    /**
     * Get referringUrl
     *
     * @return string
     */
    public function getReferringUrl()
    {
        return $this->referringUrl;
    }
}
