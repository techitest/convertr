<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Link
 *
 * @ORM\Table(name="link")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\LinkRepository")
 */
class Link
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="destination", type="string", length=255)
     */
    private $destination;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createTs", type="datetime")
     */
    private $createTs;

    /**
     * Link may be clicked multiple times
     *
     * @ORM\OneToMany(targetEntity="Click", mappedBy="link")
     */
    private $clicks;

    /**
     * Link may be have many impressions
     *
     * @ORM\OneToMany(targetEntity="Impression", mappedBy="link")
     */
    private $impressions;

    /**
     * Link may be have many impressions
     *
     * @ORM\OneToOne(targetEntity="Stats", mappedBy="link")
     */
    private $stats;

    public function __construct()
    {
        $this->clicks = new ArrayCollection();
        $this->impressions = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Link
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set destination
     *
     * @param string $destination
     *
     * @return Link
     */
    public function setDestination($destination)
    {
        $this->destination = $destination;

        return $this;
    }

    /**
     * Get destination
     *
     * @return string
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * Set createTs
     *
     * @param \DateTime $createTs
     *
     * @return Link
     */
    public function setCreateTs($createTs)
    {
        $this->createTs = $createTs;

        return $this;
    }

    /**
     * Get createTs
     *
     * @return \DateTime
     */
    public function getCreateTs()
    {
        return $this->createTs;
    }

    /**
     * Add click
     *
     * @param \AppBundle\Entity\Click $click
     *
     * @return Link
     */
    public function addClick(\AppBundle\Entity\Click $click)
    {
        $this->clicks[] = $click;

        return $this;
    }

    /**
     * Remove click
     *
     * @param \AppBundle\Entity\Click $click
     */
    public function removeClick(\AppBundle\Entity\Click $click)
    {
        $this->clicks->removeElement($click);
    }

    /**
     * Get clicks
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getClicks()
    {
        return $this->clicks;
    }

    /**
     * Add impression
     *
     * @param \AppBundle\Entity\Impression $impression
     *
     * @return Link
     */
    public function addImpression(\AppBundle\Entity\Impression $impression)
    {
        $this->impressions[] = $impression;

        return $this;
    }

    /**
     * Remove impression
     *
     * @param \AppBundle\Entity\Impression $impression
     */
    public function removeImpression(\AppBundle\Entity\Impression $impression)
    {
        $this->impressions->removeElement($impression);
    }

    /**
     * Get impressions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImpressions()
    {
        return $this->impressions;
    }
    

    /**
     * Set stats
     *
     * @param \AppBundle\Entity\Stats $stats
     *
     * @return Link
     */
    public function setStats(\AppBundle\Entity\Stats $stats = null)
    {
        $this->stats = $stats;

        return $this;
    }

    /**
     * Get stats
     *
     * @return \AppBundle\Entity\Stats
     */
    public function getStats()
    {
        return $this->stats;
    }
}
