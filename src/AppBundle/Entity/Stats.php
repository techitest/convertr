<?php

namespace AppBundle\Entity;

use AppBundle\AppBundle;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Stats
 *
 * @ORM\Table(name="stats")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StatsRepository")
 */
class Stats
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createTs", type="datetime")
     */
    private $createTs;

    /**
     * @var int
     *
     * @ORM\Column(name="impressions", type="integer")
     */
    private $impressions;

    /**
     * @var int
     *
     * @ORM\Column(name="clicks", type="integer")
     */
    private $clicks;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedTs", type="datetime")
     */
    private $updatedTs;

    /**
     * @var \AppBundle\Entity\Link
     *
     * @ORM\OneToOne(targetEntity="Link", inversedBy="stats")
     * @ORM\JoinColumn(name="linkId", referencedColumnName="id")
     */
    private $link;


    public static function addClick(Link $link, $doctrine)
    {
        $_this = (new self());
        $now = new \Datetime;
        $linkStats = $link->getStats();

        $clicks = $link->getStats()->getClicks();
        $createTs = $link->getStats()->getCreateTs();
        $clicksCount = count($link->getClicks());
        $impressionsCount = count($link->getImpressions());

        $statsEntity = $linkStats ?: $_this;

        $statsEntity->setImpressions($impressionsCount)
            ->setClicks($clicksCount)
            ->setLink($link)
            ->setCreateTs($now)
            ->setUpdatedTs($now);

        $em = $doctrine->getManager();
        $em->persist($statsEntity);
        $em->flush();

        return $_this;
    }

    public static function addImpression(Link $link, $doctrine)
    {
        $_this = (new self());

        if ($link->getStats()) return $_this->increaseImpressions($link, $doctrine);

        $now = new \Datetime;
        $clicks = count($link->getImpressions());
        $firstImpression = count($link->getImpressions());

        $_this->setImpressions($firstImpression)
            ->setClicks($clicks)
            ->setLink($link)
            ->setCreateTs($now)
            ->setUpdatedTs($now);

        $em = $doctrine->getManager();
        $em->persist($_this);
        $em->flush();

        return $_this;
    }

    private function increaseImpressions(Link $link, $doctrine)
    {
        $now = new \Datetime;
        $linkStats = $link->getStats();
        $clicks = $link->getStats()->getClicks();
        $createTs = $link->getStats()->getCreateTs();

        $impressionsCount = count($link->getImpressions());

        $linkStats->setImpressions($impressionsCount)
            ->setClicks($clicks)
            ->setLink($link)
            ->setCreateTs($createTs)
            ->setUpdatedTs($now);

        $em = $doctrine->getManager();
        $em->persist($linkStats);
        $em->flush();

        return $linkStats;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $createTs
     *
     * @return Stats
     */
    public function setCreateTs($createTs)
    {
        $this->createTs = $createTs;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getCreateTs()
    {
        return $this->createTs;
    }

    /**
     * Set impressions
     *
     * @param integer $impressions
     *
     * @return Stats
     */
    public function setImpressions($impressions)
    {
        $this->impressions = $impressions;

        return $this;
    }

    /**
     * Get impressions
     *
     * @return int
     */
    public function getImpressions()
    {
        return $this->impressions;
    }

    /**
     * Set clicks
     *
     * @param integer $clicks
     *
     * @return Stats
     */
    public function setClicks($clicks)
    {
        $this->clicks = $clicks;

        return $this;
    }

    /**
     * Get clicks
     *
     * @return int
     */
    public function getClicks()
    {
        return $this->clicks;
    }

    /**
     * Set udpatedTs
     *
     * @param \DateTime $updatedTs
     *
     * @return Stats
     */
    public function setUpdatedTs($updatedTs)
    {
        $this->updatedTs = $updatedTs;

        return $this;
    }

    /**
     * Get udpatedTs
     *
     * @return \DateTime
     */
    public function getUpdatedTs()
    {
        return $this->updatedTs;
    }

    /**
     * Set link
     *
     * @param \AppBundle\Entity\Link $link
     *
     * @return Stats
     */
    public function setLink(Link $link = null)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return \AppBundle\Entity\Link
     */
    public function getLink()
    {
        return $this->link;
    }
}
