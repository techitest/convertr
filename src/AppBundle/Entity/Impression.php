<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


/**
 * Impression
 *
 * @ORM\Table(name="impression")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ImpressionRepository")
 */
class Impression
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string", length=255)
     */
    private $ip;

    /**
     * @var string
     *
     * @ORM\Column(name="referringUrl", type="string", length=255)
     */
    private $referringUrl;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createTs", type="datetime")
     */
    private $createTs;

    /**
     * @ORM\ManyToOne(targetEntity="Link", inversedBy="impressions")
     * @ORM\JoinColumn(name="linkId", referencedColumnName="id")
     */
    private $link;


    public static function add(Link $link, $ip, $referringUrl, $doctrine)
    {
       $_this = (new self())
           ->setLink($link)
           ->setIp($ip)
           ->setReferringUrl($referringUrl)
           ->setCreateTs(new \Datetime);

        $em = $doctrine->getManager();
        $em->persist($_this);
        $em->flush();

       return $_this;
    }

    public function __construct()
    {
        $this->link = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ip
     *
     * @param string $ip
     *
     * @return Impression
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set referringUrl
     *
     * @param string $referringUrl
     *
     * @return Impression
     */
    public function setReferringUrl($referringUrl)
    {
        $this->referringUrl = $referringUrl;

        return $this;
    }

    /**
     * Get referringUrl
     *
     * @return string
     */
    public function getReferringUrl()
    {
        return $this->referringUrl;
    }
    /**
     * Set createTs
     *
     * @param \DateTime $createTs
     *
     * @return Link
     */
    public function setCreateTs($createTs)
    {
        $this->createTs = $createTs;

        return $this;
    }

    /**
     * Get createTs
     *
     * @return \DateTime
     */
    public function getCreateTs()
    {
        return $this->createTs;
    }

    /**
     * Set link
     *
     * @param \AppBundle\Entity\Link $link
     *
     * @return Impression
     */
    public function setLink(Link $link = null)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return \AppBundle\Entity\Link
     */
    public function getLink()
    {
        return $this->link;
    }
}
