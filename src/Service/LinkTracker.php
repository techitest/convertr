<?php

namespace Service;


use AppBundle\Entity\Click;
use AppBundle\Entity\Impression;
use AppBundle\Entity\Link;
use AppBundle\Entity\Stats;
use Symfony\Component\HttpFoundation\Request;

class LinkTracker
{
    /**
     * @var Request
     */
    private $request;

    private $ip;
    private $referringUrl;
    private $em;

    /**
     * @var Link
     */
    private $link;

    public function __construct(Link $link, Request $request, $doctrine)
    {
        $this->request = $request;
        $this->ip = $request->getClientIp();
        $this->referringUrl = $request->server->get('HTTP_REFERER') ?: 'None';
        $this->link = $link;
        $this->em = $doctrine;
    }

    public function trackClick()
    {
        $click = Click::add(
            $this->link,
            $this->ip,
            $this->referringUrl,
            $this->em
        );

        return Stats::addClick($this->link, $this->em);

    }

    public function trackImpression()
    {
        $impression = Impression::add(
            $this->link,
            $this->ip,
            $this->referringUrl,
            $this->em
        );

        return Stats::addImpression($this->link, $this->em);
    }

}